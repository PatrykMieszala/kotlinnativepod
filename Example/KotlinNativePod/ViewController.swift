//
//  ViewController.swift
//  KotlinNativePod
//
//  Created by Patryk Mieszała on 10/17/2018.
//  Copyright (c) 2018 Patryk Mieszała. All rights reserved.
//

import UIKit
import KotlinNativeFramework

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let funtext = LibKt.acceptFun { (lol) -> String? in
            lol + " dupa"
        }
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 300, height: 21))
        label.center = CGPoint(x: 160, y: 285)
        label.textAlignment = .center
        label.font = label.font.withSize(25)
        label.text = funtext
        label.sizeToFit()
        
        view.addSubview(label)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

