#
# Be sure to run `pod lib lint KotlinNativePod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|
  spec.name             = 'KotlinNativePod'
  spec.version          = '0.1.0'
  spec.summary          = 'A short description of KotlinNativePod.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  spec.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  spec.homepage         = 'https://github.com/Patryk Mieszała/KotlinNativePod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  spec.license          = { :type => 'MIT', :file => 'LICENSE' }
  spec.author           = { 'Patryk Mieszała' => 'patryk.mieszala@gmail.com' }
  spec.source           = { :git => 'https://github.com/Patryk Mieszała/KotlinNativePod.git', :tag => spec.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  spec.ios.deployment_target = '9.0'

  spec.source_files = 'KotlinNativeFramework/**/*'
  
  # s.resource_bundles = {
  #   'KotlinNativePod' => ['KotlinNativePod/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  
  spec.vendored_frameworks = 'KotlinNativeFramework.framework'
  
  # spec.script_phase = {
  #     :name => 'Kotlin compile',
  #     :script => 'kotlinc-native lib.kt -produce framework -target ios_arm64 -output KotlinNativeFramework',
  #     :execution_position => :before_compile,
  #     :shell_path => '/kotlin-native/bin'
  # }
  
end
