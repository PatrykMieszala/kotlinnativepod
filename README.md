# KotlinNativePod

[![CI Status](https://img.shields.io/travis/Patryk Mieszała/KotlinNativePod.svg?style=flat)](https://travis-ci.org/Patryk Mieszała/KotlinNativePod)
[![Version](https://img.shields.io/cocoapods/v/KotlinNativePod.svg?style=flat)](https://cocoapods.org/pods/KotlinNativePod)
[![License](https://img.shields.io/cocoapods/l/KotlinNativePod.svg?style=flat)](https://cocoapods.org/pods/KotlinNativePod)
[![Platform](https://img.shields.io/cocoapods/p/KotlinNativePod.svg?style=flat)](https://cocoapods.org/pods/KotlinNativePod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KotlinNativePod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KotlinNativePod'
```

## Author

Patryk Mieszała, patryk.mieszala@gmail.com

## License

KotlinNativePod is available under the MIT license. See the LICENSE file for more info.
